# Awesome Federated Content Search (FCS) @ Text+ [![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

A curated list of awesome FCS frameworks, libraries, software, resources and papers developed and published by [Text+](https://text-plus.org/).

NOTE: see original **Awesome FCS** list by CLARIN for full list of material created in CLARIN context (not duplicated here): [github.com/clarin-eric/awesome-fcs](https://github.com/clarin-eric/awesome-fcs)

<!-- see https://github.com/sindresorhus/awesome-lint -->
<!--lint disable double-link-->

## Contents

- [Websites](#websites)
- [Working Documents](#working-documents)
- [Specification Documents](#specification-documents)
- [Publications](#publications)
- [Guides and Tutorials](#guides-and-tutorials)
- [Endpoint Implementations](#endpoint-implementations)
- [Client Implementations](#client-implementations)
- [Query Parsers](#query-parsers)

## Websites

*Websites about FCS.*

- [Official FCS SRU Aggregator at Text+](https://fcs.text-plus.org/) - Main FCS client and frontend for users.
- [Text+ web portal](https://text-plus.org/) - Includes FCS search dialog.

## Working Documents

*Resources, rolling minutes and working documents.*

- [Cloud: AG FCS](https://textplus.sync.academiccloud.de/f/113779) - FCS working group.
- [Cloud: TA Lexical Resources - AG LexFCS](https://textplus.sync.academiccloud.de/f/267646)
- [Cloud: TA Lexical Resources - M1 Reference Implementation](https://textplus.sync.academiccloud.de/f/5497)
- [Cloud (Document): TA Lexical Resources - Query Szenarios](https://textplus.sync.academiccloud.de/f/137638)
- [Cloud: TA Collections - AG Reference Implementation and Portfolio Development](https://textplus.sync.academiccloud.de/f/5467)

## Specification Documents

*Specification documents related to FCS and related technologies.*

- [FCS Specification Document Sources for LexFCS extension (AsciiDoc)](https://gitlab.gwdg.de/textplus/ag-fcs-documents) - Also contains XSD schema files and examples.
- [LexFCS Specification](https://zenodo.org/records/7849753) - First publication draft for LexFCS.

## Publications

*Academic publications.*

- [Text+ publications at Zotero](https://www.zotero.org/groups/4533881/textplus/tags/FCS/library) - Lists all publications tagged with _FCS_ created in Text+.
- [Paper: _A Federated Search and Retrieval Platform for Lexical Resources in Text+ and CLARIN_ @ eLex2023](https://elex.link/elex2023/wp-content/uploads/69.pdf)
- [Poster: _Föderierte Suche in Forschungsdaten_ @ Text+ Plenary 2022](https://zenodo.org/records/7252860#.Y704xuyZOBQ)
- [Poster: _Federated Content Search - Past, Present and Future_ @ Bazaar @ CLARIN2023](https://www.clarin.eu/sites/default/files/CLARIN2023_Bazaar_29.pdf)

## Guides and Tutorials

*Guides and Tutorial about FCS.*

- *WIP* [**T**ext+ **R**esource and **S**earch **I**ntegration **H**ackathon](https://events.gwdg.de/e/TRSIH) 
  [[Blog](https://textplus.hypotheses.org/7363)]

## Endpoint Implementations

*Endpoint implementations in various languages for different search backends.*

- [LexFCS Solr Endpoint @ SAW (Java)](https://git.saw-leipzig.de/text-plus/FCS/lexfcs-solr-endpoint), 
  [[LexFCS Solr Deployment](https://git.saw-leipzig.de/text-plus/FCS/lexfcs-solr)] 
  \- FCS endpoint using Solr as search backend with focus on lexical resources.
- [FCS NoSketchEngine Endpoint @ SAW (Java)](https://github.com/Leipzig-Corpora-Collection/fcs-noske-endpoint) - FCS endpoint using NoSketchEngine as searcher for Basic and Advanced search of full texts.
- [FCS Endpoint @ DDB (Java)](https://github.com/mbuechner/dzp-fcs)
- [FCS Endpoint @ Hamburg (Python)](https://gitlab.rrz.uni-hamburg.de/timarkh/fcs-clarin-endpoint-hamburg)
- [FCS Endpoint @ TextGrid (Python)](https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint)

## Client Implementations

*FCS client implementation to query endpoints.*

- [Text+ FCS SRU Aggregator (Java)](https://gitlab.gwdg.de/textplus/ag-fcs-lex-fcs-aggregator)

*Alternative FCS client frontends, rely on FCS SRU Aggregator API.*

- [Vue Pinia Store to communicate with *FCS SRU Aggregator* REST API (JavaScript)](https://git.saw-leipzig.de/text-plus/FCS/textplus-fcs-store/)
- [Vuetify FCS search dialog (JavaScript)](https://git.saw-leipzig.de/text-plus/FCS/textplus-fcs-vuetify/) 
[[Demo @ SAW](https://tppssi-demo.saw-leipzig.de/)] 
\- Web component for integration into Text+ web portal. Includes search dialog for web portal content search and FCS.


## Query Parsers

*Parsers and converters for FCS query languages.*

- [Contextual Query Language (CQL) parser (JavaScript)](https://gitlab.gwdg.de/textplus/ag-fcs-lex-fcs-cql-js) - A hand-written, recursive descent parser of the Contextual Query Language as
used in the standardized information retrieval protocols - SRU. Fork that allows installation with `npm` and adds some additional functionality.
- [LexCQL Validator (and Parser) (Java)](https://gitlab.gwdg.de/textplus/ag-fcs-lex-fcs-lexcql-java) - Small wrapper around `cql-java` for CQL parsing, adds validation for LexCQL (CQL for Lexical Resources).
- [CQL to XCQL Transformer (XQuery)](https://github.com/digicademy/cql-parser-xqm) - A CQL parser written in XQuery that can transform any CQL query into XCQL .
